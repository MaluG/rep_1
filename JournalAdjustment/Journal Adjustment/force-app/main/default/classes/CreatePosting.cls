public with sharing class CreatePosting {
    @AuraEnabled
    public static Posting createPosting(Posting post) {
        system.debug('posting'+post);
        insert post;
        
        return post;
    }
}
