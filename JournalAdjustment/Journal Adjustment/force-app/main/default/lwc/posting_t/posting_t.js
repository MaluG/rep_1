import { LightningElement, api} from 'lwc';
import POSTING_T_OBJECT from '@salesforce/schema/Posting_t__c';
import CONNECTED_ACCOUNT_FIELD from '@salesforce/schema/Posting_t__c.Related_Account__c';
import AMOUNT_FIELD from '@salesforce/schema/Posting_t__c.Amount__c';
import LEDGER_ENTRY_FIELD from '@salesforce/schema/Posting_t__c.Ledger_entry__c';

export default class LedgerEntryCreator extends LightningElement {
    objectApiName = POSTING_T_OBJECT;
    fields = [CONNECTED_ACCOUNT_FIELD, AMOUNT_FIELD];
    @api recordId;
    handleSubmit(event){
      
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
        fields.Ledger_entry__c = this.recordId; // modify a field
        this.template.querySelector('lightning-record-form').submit(fields);
     }
}