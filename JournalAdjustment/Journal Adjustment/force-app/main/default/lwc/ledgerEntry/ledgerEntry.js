import { LightningElement, api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { encodeDefaultFieldValues } from 'lightning/pageReferenceUtils';
import LEDGER_ENTRY_T_OBJECT from '@salesforce/schema/Ledger_Entry__c';


export default class LedgerEntryCreator extends NavigationMixin (LightningElement){
    
    @api recordId;

    objectApiName = LEDGER_ENTRY_T_OBJECT;

    defaultValues = encodeDefaultFieldValues({
        Ledger_entry__c: this.recordId
    });

        navigateToNewPosting() {
            this[NavigationMixin.Navigate]({
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Posting_t__c',
                    actionName: 'new'
                },
            //     state: {
            //          defaultFieldValues: defaultValues
            //  }
        });
    }
    
    navigateToNewLedgerEntry(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Ledger_Entry__c',
                actionName: 'new'
            },
        });

    }
}
