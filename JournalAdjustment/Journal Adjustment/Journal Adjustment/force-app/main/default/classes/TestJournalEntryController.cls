@isTest
public with sharing class TestJournalEntryController {
    public TestJournalEntryController() {

        
    }
    // @isTest
    // public static void sanityTest1(){
    //     String testStr = '{"debitAccount":"0014K00000BoCu1QAF","creditAccount":"0014K00000BoDFhQAN",'+
    //     '"originalCurrency":"USD","sttlCurrency":"USD","originalCurrAmount":"1000","sttlCurrAmount":'+
    //     '"1000","paymentDate":"2021-01-01T08:00:00.000Z","bankRef":"Poalim","'+
    //     'description":"End of year payment","exchangeRate":"1.0"}';

    //     Ledger_Entry__c testLe = JournalEntryController.createJournalEntry(teststr);
    //     List<Posting__c> result = getPostings(testLe.id);
    //     System.debug(result);

    //     for(Posting__c post : result ){
        
    //         System.debug(post.id);
    //         System.debug('connected LE' + post.Ledger_Entry__c);

    //         System.assertEquals(testLe.id, post.Ledger_Entry__c);
    //     }

    // }

    // @isTest
    // public static void sanityTest2(){
    //     String testStr = '{"debitAccount":"0014K00000AxrDLQAZ","creditAccount":"0014K00000Bq8anQAB",'+
    //     '"originalCurrency":"USD","sttlCurrency":"EUR","originalCurrAmount":"1000","sttlCurrAmount":'+
    //     '"818.710","paymentDate":"2021-01-01T08:03:00.000Z","bankRef":"Morgan","'+
    //     'description":"End of year payment","exchangeRate":"1.22"}';

    //     Ledger_Entry__c testLe = JournalEntryController.createJournalEntry(teststr);
    //     List<Posting__c> result = getPostings(testLe.id);
    //     System.debug(result);

    //     for(Posting__c post : result ){
        
    //         System.debug(post.id);
    //         System.debug('connected LE' + post.Ledger_Entry__c);

    //         System.assertEquals(testLe.id, post.Ledger_Entry__c);
    //     }

    // }

   

    @isTest
    public static void sanityTest3(){
        String testStr = '{"debitAccount":"0014K00000BoDFhQAN","creditAccount":"0014K00000Bq8anQAB",'+
        '"originalCurrency":"USD","sttlCurrency":"USD","originalCurrAmount":"2000","sttlCurrAmount":"2000",'+
        ' "paymentDate":"2021-02-04T16:15:00.000Z","bankRef":"Morgan","description":"","exchangeRate":1}';

        Ledger_Entry__c testLe = JournalEntryController.createJournalEntry(teststr);
        List<Posting__c> result = getPostings(testLe.id);
        System.debug(result);

        for(Posting__c post : result ){
        
            System.debug(post.id);
            System.debug('connected LE' + post.Ledger_Entry__c);

            System.assertEquals(testLe.id, post.Ledger_Entry__c);
        }

    }
    public static List<Posting__c> getPostings(ID currLedgerEntry){

        List<Posting__c> result = [SELECT id,Ledger_Entry__c  FROM Posting__c WHERE Ledger_Entry__c =: currLedgerEntry]; 
        
        return result;
    }

   
}

