public with sharing class JournalEntryController {
    
    @AuraEnabled
    public static Ledger_Entry__c createJournalEntry(String source){

        System.System.debug(source);

        Ledger_Entry__c ledgerEntry = new Ledger_Entry__c(Transaction_Summary__c=0);

        try{
            insert ledgerEntry;
        }catch(Exception e){
            System.debug('--->'+e);
        }
        
        //TODO change to List<Posting_c>
        Posting__c debit = createPosting(ledgerEntry.id, source, 'debit');
        insert debit;
        Posting__c credit = createPosting(ledgerEntry.id, source, 'credit');
        insert credit;

        return ledgerEntry;

    }

    private static Posting__c createPosting(ID id, String src, String accountType){

        Map<String,Object> resmap = (Map<String, Object>) JSON.deserializeUntyped(src);
        Decimal creditOrgAmount = 0;
        Decimal debitOrgAmount = 0;
        Decimal creditSttlAmount = 0;
        Decimal debitSttlmount = 0;
        ID account;

        Decimal originalCurrAmount = Decimal.valueof((String) resmap.get('originalCurrAmount'));
        Decimal sttlCurrAmount =  Decimal.valueof((String) resmap.get('sttlCurrAmount'));
        String originalCurr = (String) resmap.get('originalCurrency');
        String sttlCurrency = (String) resmap.get('sttlCurrency');
        String dateStr = (String) resmap.get('paymentDate');
        Datetime paymentDate = (Datetime)JSON.deserialize(dateStr, DateTime.class);
        String bank = (String) resmap.get('bankRef');
        String description =  (String) resmap.get('description');
        Decimal exchange = (Decimal) resmap.get('exchangeRate');
        String transactionSum = (String) resmap.get('tSummary');

        if(accountType == 'debit'){
            String debitStr = (String)resmap.get('debitAccount');  
            account = (ID)debitStr;
            debitOrgAmount = originalCurrAmount;
            debitSttlmount = sttlCurrAmount;

        }
        else{
            String creditStr = (String)resmap.get('creditAccount');  
            account = (ID)creditStr;
            creditOrgAmount = originalCurrAmount;
            creditSttlAmount = sttlCurrAmount;
        }
       
        
        
        Posting__c post = new Posting__c(
                         Amount_in_original_currency__c=originalCurrAmount,
                         Amount_in_settlement_currency__c=sttlCurrAmount,
                         Credit_amount_in_original_currency__c=creditOrgAmount,
                         Debit_amount_in_original_currency__c=debitOrgAmount,
                         Credit_amount_in_settlement_currency__c=creditSttlAmount,
                         Debit_amount_in_settlement_currency__c=debitSttlmount,
                         Bank_Reference__c=bank,
                         Connected_Account__c=account,
                         Description__c=description,
                         Exchange_Rate__c=exchange,
                         Ledger_Entry__c=id,
                         Original_Currency__c=originalCurr,
                         Settlement_Currency__c=sttlCurrency,
                         Payment_date__c=paymentDate,
                         Posting_Type__c='JNL'
                         );

        system.debug(post.Ledger_Entry__c);                
        return post;
    }

}
