import { LightningElement,wire } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import CURRENCY from '@salesforce/schema/Posting__c.Original_Currency__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import POSTING__C_OBJECT from '@salesforce/schema/Posting__c';

export default class LwcPicklistWithoutRecordtype extends LightningElement {
    @wire(getObjectInfo, { objectApiName: POSTING__C_OBJECT })
    postingInfo;
   
    @wire(getPicklistValues,
        {
            recordTypeId: '$postingInfo.data.defaultRecordTypeId',
            fieldApiName: CURRENCY
        }
    )
    currencyValues;

    handleChange(event) {
        // Creates the event
        const selectedEvent = new CustomEvent('valueselected', {
            detail: event.detail.value,
            bubbles: true
        });
        //dispatching the custom event
        this.dispatchEvent(selectedEvent);
    }
}