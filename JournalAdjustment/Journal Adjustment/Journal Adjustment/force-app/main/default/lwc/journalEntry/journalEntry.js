import { LightningElement, track} from 'lwc';
import createJournalEntry from '@salesforce/apex/JournalEntryController.createJournalEntry'
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
 

export default class JournalEntry extends LightningElement {

    selectedRecordId; //store the record id of the selected 
   
   @track state = {
        debitAccount: '',
        creditAccount: '',
        originalCurrency : '',
        sttlCurrency : '',
        originalCurrAmount : '',
        sttlCurrAmount : '',
        paymentDate: new Date().toISOString(),
        bankRef : '',
        description: '',
        exchangeRate:0,
    }

    selectedId(event){
        this.selectedRecordId = event.detail[0];

        return(this.selectedRecordId);
    }

   
    handleDebitSelect(event){
        if(this.selectedId(event)){
            this.state.debitAccount =  this.selectedRecordId;
            console.log('debit = ' + JSON.stringify(this.state.debitAccount));
        }     
    }

    handleCreditSelect(event){
        if(this.selectedId(event)){
            this.state.creditAccount =  this.selectedRecordId;
            console.log('credit = ' + JSON.stringify(this.state.creditAccount));
        }
    }


    handleSelect(event){
        this.state[event.target.dataset.id] = event.detail.value;
    }
  
    
    handleChange(event){
        this.state[event.target.dataset.id] = event.target.value;
    }


    get exchangeRate(){

        // console.log("original amount = " + this.state.originalCurrAmount);
        // console.log("sttl amount = " + this.state.sttlCurrAmount);

        if(this.state.originalCurrAmount && this.state.originalCurrAmount !== '0' && 
            this.state.sttlCurrAmount && this.state.sttlCurrAmount !== '0'){

            return this.state.exchangeRate = 
            (parseFloat(this.state.originalCurrAmount) / parseFloat(this.state.sttlCurrAmount));
        }
               
       return 0;
    }

    
    createJE(){

        let jsonStr = JSON.stringify(this.state);

        console.log("calling apex method with:" + jsonStr);
        
        createJournalEntry({source : jsonStr}).
        then((result) => {
            console.log('result = ' + result);

            this.dispatchEvent(new ShowToastEvent({
                title: 'Success!!',
                message: 'Jornal Entry Created Successfully!!',
                variant: 'success'
            }), );
            console.log(JSON.stringify(result));

        })
        .catch((error) => {
            console.log('error = '+ error);
        })

        console.log("after stringify");
    }
}

