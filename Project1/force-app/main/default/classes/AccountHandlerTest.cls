@IsTest
public with sharing class AccountHandlerTest {
    public AccountHandlerTest() {

    }
    @IsTest
    //Should create account if a name was given, 
    //in case name == null should return null
    private static void shouldCreateAccount(){
      
        Account result = AccountHandler.insertNewAccount('Bobby');
        System.assertEquals('Bobby', result.name);

    }
  
}
