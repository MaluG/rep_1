@isTest
public class TestRestrictContactByName {
    
    @isTest
    private static void testTriggerMethod(){
        
        List<Contact> contactList = new List<Contact>();
		contactList.add(new Contact(LastName='testiContactName'));
        contactList.add(new Contact(LastName='testiContactName2'));
        contactList.add(new Contact(LastName='testiContactName3'));
        
        Contact invalidContact =  new Contact(LastName='INVALIDNAME');
              
        Test.startTest();
        Database.SaveResult[] srList = Database.insert(contactList, false);
        Database.SaveResult srInvalid = Database.insert(invalidContact, false);
        
        Test.stopTest();
        
        for (Database.SaveResult sr : srList) {
   			system.assert(sr.isSuccess());
   		 }
        
    	
        system.assert(!srInvalid.isSuccess());
        System.assert(srInvalid.getErrors().size() > 0);
        System.assertEquals('The Last Name "'+invalidContact.LastName+'" is not allowed for DML',
                             srInvalid.getErrors()[0].getMessage());
        
    }

}