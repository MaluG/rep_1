public with sharing class AccountHandler {
    public AccountHandler() {

    }

    public static Account insertNewAccount(String name){

        Account account = new Account(Name=name);
        try{
            insert account;

        }catch(DmlException e){
            System.debug('A DML exception has occurred: ' +
            e.getMessage());
            return null;
        }

        return account;
    }
}
