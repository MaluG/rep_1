
public class RandomContactFactory {
    
     public static List<Contact> generateRandomContacts(Integer numContacts, String lastName){

        List<Contact> contacts = new List<Contact> ();

        for(Integer i=0;i<numContacts;i++) {
            Contact a = new Contact(LastName = lastName, FirstName ='Test' + i);
            contacts.add(a);
        }
        
        return contacts;
     }


}