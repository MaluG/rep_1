@IsTest
private with sharing class ManageContactsTest {
    @IsTest
    static void shouldDeleteAllContacts(){

        List<Contact> conList = new List<Contact> {
            new Contact(FirstName='Harry',LastName='Potter',Department='Gryffindor'),
                new Contact(FirstName='Hagrid',LastName='Smith',Department='Maintenance'),
                new Contact(FirstName='Ron',LastName='Weasley',Department='Gryffindor'),
                new Contact(FirstName='Albus',LastName='Dumbledore',Department='Education')};
                    
        insert conList;
       
        Contact[] contactsDel = [SELECT Id, Name FROM Contact]; 
        system.assert(!contactsDel.isEmpty());

        ManageContacts.clearAllContacts();
        contactsDel = [SELECT Id FROM Contact]; 
        system.assert(contactsDel.isEmpty());

    }
}
