public with sharing class ContactSearch {
    public ContactSearch() {

    }

    public static List<Contact> searchForContacts(String last_name, String postal_code){
        
        List<Contact> result = [SELECT id, name From Contact
                            WHERE LastName =: last_name
                           AND MailingPostalCode =: postal_code];
            return result;
    }
}
